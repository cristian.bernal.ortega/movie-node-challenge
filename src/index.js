const Koa = require('koa');
const joi = require('joi');
const Router = require('koa-router');
const koaRequest = require('koa-http-request');
const apikey = "e0cea321";
const logger = require('koa-logger')

const mongoose = require('mongoose');
const { string } = require('joi');

//conección a la base de datos 
mongoose.connect('mongodb+srv://user:Lg5uzu8ksDNOSFbW@clusterdelcoso.jehas.mongodb.net/myFirstDatabase', { useNewUrlParser: true, useUnifiedTopology: true });

//modelo utilizado para almacenar y traer informacion de la base de datos
const Movie = mongoose.model('Movie', { imdbid: String, title: String, year: String, released: String, genre: String, director: String, actors: String, plot: String, ratings: [] });

const app = new Koa();
const router = new Router();

app.use(koaRequest({
  json: true, //automatically parsing of JSON response
  timeout: 3000,    //3s timeout
  host: 'http://www.omdbapi.com/'
}));

//Primer requerimiento
router.get('/movies', async ctx => {
  try {
    var id = ctx.header["id"];
    let data = await ctx.get('/?apikey=' + apikey + '&i=' + id, null, {
      'User-Agent': 'koa-http-request'
    });
    var movie = new Movie({ imdbid: data.imdbID, title: data.Title, year: data.Year, released: data.Released, genre: data.Genre, director: data.Director, actors: data.Actors, plot: data.Plot, ratings: [data.Ratings] });
    movie.save().then(() => console.log('gotcha'));
    ctx.body = movie;
  } catch (error) {
    ctx.body = 'ocurrio un error' + error;
    ctx.status = 500;
  }
});

/*router.post('/movies', async ctx => {
  ctx.body = 'Hola desde el contact.';
});*/

//prueba del get all

router.get('/movies/lista', async ctx => {
  var listar = {};
  const all = await Movie.find(listar);
  ctx.body = all;
});


app.use(router.routes());

app.use(async ctx => {
  return (ctx.body = 'Ruta no encontrada.');
});

app.use(logger())

//inicio del servidor en localhost:3000
app.listen(3000);